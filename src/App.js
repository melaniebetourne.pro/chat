import React, { Component } from "react";
import Output from "./Component/Output";
import Input from "./Component/Input";
import Chat from "./Class/Chat";
import Connecter from "./Component/Connecter";
import Login from "./Component/Login";
import "./App.css";
import Socket from "./Component/Socket";

import Disconnect from "./Component/Disconnect";
import User from "./Class/User";
import "./Responsive.css"
import Swal from "sweetalert2";




const INIT        = 0;
const VALIDMASTER = 1;
const MESSAGE     = 3;
const UPDATELOGIN = 4;
const ERRORLOGIN = 5;

class App extends Component {
  constructor(props) {
    super(props);

    this.echange = new Chat();

    this.state = { modif: false, message: "{}", user: "{}", etat: INIT};
    this.address=window.location.href;
    this.address=this.address.substring(0,this.address.length-5)+"5000";
    Socket.initsocket(this.address);
}

componentDidMount() {

  // configuration réception message add
  Socket.configuresocket((err, data) => {
    
    let jsonReceive = JSON.parse(data);

    if (jsonReceive[0].type === VALIDMASTER && this.state.etat === INIT){
      this.echange.messages = [];
      console.log("historique des messages clients", jsonReceive[1].messages);
      jsonReceive[1].messages.map((msg) => this.setState({ message: msg, etat: VALIDMASTER}));
    }
    else if (jsonReceive[0].type === MESSAGE){
      this.setState({ message: jsonReceive[1]});
      //Local Storage
      localStorage.setItem('myHistoryMessage', JSON.stringify(this.echange.messages));
      console.log('local Storage client',localStorage.getItem('myHistoryMessage'));
    }
    // Reception de la Mise à jour des users 
    if(jsonReceive[0].type === UPDATELOGIN){
      this.echange.users = [];
      for (let i = 0; i < jsonReceive[1].user.length; i++) {
        let usr=new User();
        usr.create(jsonReceive[1].user[i].avatar,jsonReceive[1].user[i].pseudo);
        this.echange.users.push(usr);
      }
      this.cestok();
    }
    if(jsonReceive[0].type === ERRORLOGIN){
      this.echange = new Chat();
      this.echange.messages = [];
      this.echange.users = [];
      let usr=new User();
      usr.create("","");
      Swal.fire({
        title: 'Erreur !',
        text: "Cet identifiant est déjà connecté",
        type: 'error',
        confirmButtonText: 'OK'
      });
      this.cestok();
    } 

  });
}

  cestok = () => {
    this.setState({ modif: !this.state.modif });
    console.log(this.state.modif);
    console.log(this.echange);
  
  };

  traitemessage=()=> {
    if(this.state.message.length){
      if (this.echange.me.pseudo !== this.state.message[this.state.message.length -1].sender.pseudo){
        console.log(this.state.message[this.state.message.length -1]);
        this.echange.addMessage(this.state.message[this.state.message.length -1]);
      }
      this.setState({message:"{}"});
    } else {
      if (this.echange.me.pseudo !== this.state.message.sender.pseudo){
        console.log(this.state.message);
        this.echange.addMessage(this.state.message);
      }
      this.setState({message:"{}"});
    }

};

  traitePseudo=()=> {
    if (this.echange.me.pseudo !== this.state.user.pseudo){
      this.echange.addUser(this.state.user);
    }
    this.setState({user:"{}"});
  };

  render() {
    if (this.state.message !== "{}")
      this.traitemessage();
    if (this.state.user !== "{}")
      this.traitePseudo();
    if (this.echange.me.pseudo ==="") {
      console.log("Client connecté");
      return (
      <div>
        <Login source={this.echange} callback={this.cestok}/>
      </div>);
    }
    else {
      console.log("Client Loggué !");
    return (
      <div>
        <div className="navbar">
        <h1 className="navitem">Chat </h1>
        <Disconnect className="navitem"/> 
        </div>
        <div className="chat">
        <div className ="chatapp">
          <Connecter source={this.echange} callback={this.cestok}/>
        </div>
    
        <div className= "chatapp">

          <div>

           
          </div>
         
        </div>
        <div className= "chatapp">
          <Output source={this.echange}/>
          <Input source={this.echange} callback={this.cestok}/>
      
        </div>
        </div>
      </div>
      );
    }
  }
}



export default App;

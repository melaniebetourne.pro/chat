import User from "./User";

class Chat {
  constructor() {
    this.me=new User();
    this.messages = [];
    this.users = [];
    console.log(this.messages);
    console.log(this.users);
  }
  addUser(usr) {
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].pseudo === usr.pseudo) {
        return false;
      }
    }
    this.users.push(usr);
    return true;
  }
   addMessage(msg){
    if (msg.text==="") return false;
    this.messages.push(msg);
    return true;
    }
}
export default Chat;